/**
 * Infinite Scroll
 */

(function(window, document) {
  // next link element
  const nextElement = document.querySelector('link[rel=next]')
  if (!nextElement) return

  // post feed element
  const feedElement = document.querySelector('.postFeed')
  if (!feedElement) return

  let buffer = 300

  let ticking = false
  let loading = false

  let lastScrollY = window.scrollY
  let lastWindowHeight = window.innerHeight
  let lastDocumentHeight = document.documentElement.scrollHeight

  function onUpdate() {
    // return if already loading
    if (loading) return

    // return if not scroll to the bottom
    if (lastScrollY + lastWindowHeight <= lastDocumentHeight - buffer) {
      ticking = false
      return
    }

    loading = true
    fetch(nextElement.href)
      .then(res => res.text())
      .then(page => {
        // append contents
        const parser = new DOMParser()
        const doc = parser.parseFromString(page, 'text/html')

        let postElements = doc.querySelectorAll('.postCard')
        postElements.forEach(function(item) {
          feedElement.appendChild(item)
        })

        // set next link
        let resNextElement = doc.querySelector('link[rel=next]')
        if (resNextElement) {
          nextElement.href = resNextElement.href
        } else {
          window.removeEventListener('scroll', onScroll)
          window.removeEventListener('resize', onResize)
        }

        // sync status
        lastDocumentHeight = document.documentElement.scrollHeight
        ticking = false
        loading = false
      })
      .catch(() => {
        window.removeEventListener('scroll', onScroll)
        window.removeEventListener('resize', onResize)
      })
  }

  function requestTick() {
    ticking || window.requestAnimationFrame(onUpdate)
    ticking = true
  }

  function onScroll() {
    lastScrollY = window.scrollY
    requestTick()
  }

  function onResize() {
    lastWindowHeight = window.innerHeight
    lastDocumentHeight = document.documentElement.scrollHeight
    requestTick()
  }

  window.addEventListener('scroll', onScroll, { passive: true })
  window.addEventListener('resize', onResize)

  requestTick()
})(window, document)
