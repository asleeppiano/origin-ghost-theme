/*global Prism*/
/*eslint no-undef: "error"*/

const allVids = document.querySelectorAll(`
  iframe[src*="player.vimeo.com"],
  iframe[src*="youtube.com"],
  iframe[src*="youtube-nocookie.com"],
  iframe[src*="kickstarter.com"][src*="video.html"],
img[class*="tagFeatureImage"],
img[class*="authorCoverImage"]`)

const postPageContent = document.querySelector('.postPageContent')

if (postPageContent) {
  allVids.forEach(video =>
    postPageContent.getBoundingClientRect().width < 750
      ? (video.style.height = `${Math.floor(
          (postPageContent.getBoundingClientRect().width * 9) / 16
        )}px`)
      : (video.style.height = 420 + 'px')
  )
  window.addEventListener('resize', () =>
    allVids.forEach(video =>
      postPageContent.getBoundingClientRect().width < 750
        ? (video.style.height = `${Math.floor(
            (postPageContent.getBoundingClientRect().width * 9) / 16
          )}px`)
        : (video.style.height = 420 + 'px')
    )
  )
}

const images = document.querySelectorAll('.kg-gallery-image img')
images.forEach(function(image) {
  const container = image.closest('.kg-gallery-image')
  const width = image.attributes.width.value
  const height = image.attributes.height.value
  const ratio = width / height
  container.style.flex = ratio + ' 1 0%'
})

const navigationButton = document.getElementById('navigationButton')
if (navigationButton) {
  navigationButton.addEventListener('click', () => {
    const featuredPosts = document.querySelector('.featuredPosts')
    if (!navigationButton.hasAttribute('active')) navigationButton.setAttribute('active', '')
    else navigationButton.removeAttribute('active')
    featuredPosts.classList.toggle('block')
  })
}

Prism.highlightAll(true, () => {})
