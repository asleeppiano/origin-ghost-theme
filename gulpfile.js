const { series, watch, src, dest, parallel } = require('gulp')
const livereload = require('gulp-livereload')
const postcss = require('gulp-postcss')
const zip = require('gulp-zip')
const minify = require('gulp-minify')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')

function serve(done) {
  livereload.listen()
  done()
}

function hbs(done) {
  return src(['*.hbs', 'partials/**/*.hbs', '!node_modules/**/*.hbs']).pipe(livereload())
}

function css(done) {
  const processors = [autoprefixer(), cssnano()]

  return src('assets/css/*.css', { sourcemaps: true })
    .pipe(postcss(processors))
    .pipe(dest('assets/dist/', { sourcemaps: '.' }))
    .pipe(livereload())
}

function js(done) {
  return src('assets/js/*.js', { sourcemaps: true })
    .pipe(minify())
    .pipe(dest('assets/dist/', { sourcemaps: '.' }))
    .pipe(livereload())
}

function zipper(done) {
  const filename = `${require('./package.json').name}.zip`
  return src(['**', '!node_modules', '!node_modules/**', '!dist', '!dist/**'])
    .pipe(zip(filename))
    .pipe(dest('dist/'))
}

const cssWatcher = () => watch('assets/css/**', css)
const hbsWatcher = () => watch(['*.hbs', 'partials/**/*.hbs', '!node_modules/**/*.hbs'], hbs)
const jsWatcher = () => watch('assets/js/**', js)
const watcher = parallel(cssWatcher, hbsWatcher, jsWatcher)
const build = series(css, js)
const dev = series(build, serve, watcher)

exports.build = build
exports.zip = series(build, zipper)
exports.default = dev
