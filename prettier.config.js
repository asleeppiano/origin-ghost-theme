// prettier.config.js or .prettierrc.js
module.exports = {
  trailingComma: 'es5',
  tabWidth: 2,
  semi: false,
  singleQuote: true,
  printWidth: 100,
  overrides: [
    {
      files: "*.hbs",
      options: {
        tabWidth: 4
      }
    }
  ]
}
